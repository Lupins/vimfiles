filetype off                  " required
filetype indent on
filetype plugin on
syntax on

" set the runtime path to include Vundle and initialize
set nocompatible
set rtp+=~/.vim/bundle/Vundle.vim
set number
set ruler
set laststatus=2
set cursorline
set expandtab
set shiftwidth=2
set softtabstop=2
set showmode
set autoindent
set visualbell
set noerrorbells
set background=dark

autocmd Filetype tex setl updatetime=1

" allows Ctrl-s and Ctrl-q mapping
silent !stty -ixon

colorscheme solarized
let g:airline_theme='solarized'
let NERDTreeShowHidden=1

call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
" vim +PluginInstall +qall
Plugin 'VundleVim/Vundle.vim'

" NERD tree
Plugin 'scrooloose/nerdtree'
" Ruby Sintax
Bundle 'vim-ruby/vim-ruby'
" Vim Rubocop
Plugin 'ngmy/vim-rubocop'
" Vim status bar
Plugin 'vim-airline/vim-airline'
" Vim status bar theme
Plugin 'vim-airline/vim-airline-themes'
" Vim colorschemes
Plugin 'flazz/vim-colorschemes'
" Vim search trough files
" Plugin 'mileszs/ack.vim'
" Vim better grep
Plugin 'dkprice/vim-easygrep'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

:imap jj  <Esc>

:map  <silent>  <C-b> :NERDTreeToggle<CR>
:map  <silent>  <S-Tab> <C-W>w
:imap <C-s> <C-o>:up<CR>

:nmap <S-h> 0
:nmap <S-j> G
:nmap <S-k> gg
:nmap <S-l> $
:nmap <C-s> :up<CR>
:nmap <C-h> <C-w>h
:nmap <C-j> <C-w>j
:nmap <C-k> <C-w>k
:nmap <C-l> <C-w>l
